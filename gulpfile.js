'use strict';

let gulp          = require('gulp');
let zip           = require('gulp-zip');
let gzip          = require('gulp-gzip');
let fs            = require('fs');
let replace       = require('gulp-replace');
let concat        = require('gulp-concat');
let uglify        = require('gulp-uglify');
let rename        = require('gulp-rename');
let clean         = require('gulp-clean');
let less          = require('gulp-less');

gulp.task('clean', () => {
  return gulp.src([
    'build',
  ], {read: false})
    .pipe(clean());
});





gulp.task('bundle-vendor', () => {
  return gulp.src([
    'app/lib/angular.min.js',
    'app/lib/angular-route.min.js',
    'app/lib/angular-ui-router.min.js',
    'node_modules/ng-scroll/ng-scroll.js'
  ])
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(gulp.dest('build/client/js'))
});





gulp.task('bundle-commons', gulp.series('bundle-vendor', () => {
  return gulp.src([
    'app/common/background.js',
    'app/common/content.js'
  ])
    .pipe(gulp.dest('build/common'))
  })
);





gulp.task('bundle-client', gulp.series('bundle-commons', () => {
  gulp.src([
    'app/client/injection/**/*.js'
  ])
    .pipe(gulp.dest('build/client/injection/'));

  return gulp.src([
    'app/popup.js',
    'app/client/**/*.drct.js',
    'app/client/**/*.fct.js',
    'app/client/**/*.srv.js',
    'app/client/**/*.ctrl.js',
    'app/client/**/*.filter.js'
  ])
    .pipe(concat('client.js'))
    .pipe(gulp.dest('build/client/js'));
  })
);







gulp.task('bundle-style', () => {
  gulp.src([
    'app/styles/styles-reset.css',
    'app/styles/**/*.css'
  ])
    .pipe(concat('style.css'))
    .pipe(gulp.dest('build/css'));

  let cell = gulp.src('./app/styles/**/*.less')
    .pipe(less())
    .pipe(concat('less.css'))
    .pipe(gulp.dest('build/css'));
  cell.on('error', console.error.bind(console));
  return cell;
});








gulp.task('bundle-ext-files', gulp.series('bundle-style', () => {
  return gulp.src(['app/manifest.json' ])
    .pipe(gulp.dest('build/'))
  })
);






gulp.task('bundle-img', gulp.series('bundle-ext-files', () => {
  return gulp.src('app/assets/**/*.png')
    .pipe(gulp.dest('build/assets'))
  })
);







gulp.task('change-links', gulp.series('bundle-ext-files', () => {

  gulp.src('app/client/home/home.html')
    .pipe(gulp.dest('build/client/home/'))

  let data = fs.readFileSync('app/popup.html', "utf-8");
  var buff1 = data.substring(data.indexOf("<!-- Dev start -->"),data.lastIndexOf("<!-- Dev start -->"))
  var buff2 = data.substring(data.indexOf("<!-- Dev end -->"),data.lastIndexOf("<!-- Dev end -->"))


  return gulp.src('app/popup.html')
    .pipe(replace(buff1, ''))
    .pipe(replace(buff2, ''))
    .pipe(replace('<!-- Dev start -->', ''))
    .pipe(replace('<!-- Dev end -->', ''))
    .pipe(replace('<!-- Styles css -->', '<link href="css/style.css" rel="stylesheet" type="text/css">'))
    .pipe(replace('<!-- Styles less -->', '<link href="css/less.css" rel="stylesheet" type="text/css">'))
    .pipe(replace('<!-- Vendor scripts -->', '<script src="client/js/vendor.js"></script>'))
    .pipe(replace('<!-- Client scripts -->', '<script src="client/js/client.js"></script>'))
    .pipe(gulp.dest('build'))
  })
);







/* Zip application */
gulp.task('zip-app', gulp.series('change-links', () => {
  let version = JSON.parse(fs.readFileSync('app/manifest.json')).version;
  return gulp.src(['build/**/*'])
    .pipe(zip('zoomin-extension.' + version +'.zip'))
    .pipe(gulp.dest('zip'));
  })
);






/* Zip application */
gulp.task('tar-zip-app', gulp.series('zip-app', () => {
  let version = JSON.parse(fs.readFileSync('app/manifest.json')).version;
  return gulp.src('build/**/*')
    .pipe(gzip())
    .pipe(gulp.dest('zip'));
  })
);








gulp.task('build',
  gulp.series('clean', 
    gulp.parallel(
      'bundle-vendor',
      'bundle-commons',
      'bundle-client',
      'bundle-style',
      'bundle-ext-files',
      'bundle-img',
      'change-links' 
      // 'zip-app',
      // 'tar-zip-app'
    )
  )
);



/* Run server on default */
gulp.task('default', gulp.series('build'));