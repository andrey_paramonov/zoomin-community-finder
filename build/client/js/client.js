// You need to explicitly add URL protocols to Angular's whitelist using a regular expression. 
// Only http, https, ftp and mailto are enabled by default. 
// Angular will prefix a non-whitelisted URL with unsafe: when using a protocol such as chrome-extension:.
var app = angular.module("myApp", ["ui.router"]).config(['$compileProvider', function( $compileProvider ) {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
    }
]);


app.config(function($stateProvider) {

    $stateProvider
    .state('home', {
      url: '/',
      views: {
        'main': {
          controller: 'homeCtrl',
          controllerAs: 'vm',
          templateUrl: 'client/home/home.html'
        }
      }
    });

});


app.controller('myCtrl', function($state) {
    $state.go('home');
});

function homeCtrl($scope, $q, $timeout) {
    
  const vm = this;

  vm.checkSiteFired = false;
  vm.siteData       = {};
  vm.error          = '';

  vm.model = {
    isSalesforce: false,
    isCommunity: false,
    isRegular: false
  };

  vm.init = () => {
    chrome.tabs.getSelected(null,(tab) => {
      vm.addListener();
    });
  }



  // Listener
  vm.addListener = () => {
    chrome.runtime.onMessage.addListener((request, sender) => {
      if (request.action == "getInformation") {
        vm.siteData = request;
        $timeout(vm.updateParameters(), 1000);
      }
    });
  };




  // "Check Site" button press handler
  vm.checkSite = () => {
    vm.checkSiteFired = true;
    vm.loadScript();
  };





  // Loading content script
  vm.loadScript = () => {
    chrome.tabs.executeScript(null, {
      file: "client/injection/injected-parser.js"
    }, () => {
      if (chrome.runtime.lastError) {
        vm.error = 'There was an error injecting script : \n' + chrome.runtime.lastError.message;
        console.log(vm.error);
      }
    });
  };




  vm.updateParameters = () => {
    console.log('%cUpdating parameters!', 'color:red;');
    console.log(vm.siteData);

    vm.model.isCommunity = vm.checkCommunity();
    if (vm.model.isCommunity) {
      return;
    }

    vm.model.isSalesforce = vm.checkSalesforce();
    if (vm.model.isSalesforce) {
      return;
    }

    vm.model.isRegular = true;

  };


  // Check if site is community
  vm.checkCommunity = () => {
    var _isCommunity = false;
    var _filledAuraTags = 0;
    var item;

    for (item in vm.siteData.hasAuraTags) {
      if (vm.siteData.hasAuraTags[item]) {
        _filledAuraTags += 33;
      }
    }

    if (vm.siteData.isCommunityLocation && _filledAuraTags > 90) {
      _isCommunity = true;
    }

    return _isCommunity;
  };




  // Check if site is salesforce org
  vm.checkSalesforce = () => {
    var _isSalesforce = false;
    var hasSFDomain = false;
    var hasTags = false;
    var item;

    hasTags = vm.siteData.defaultSFClassicParams.isDefaultChance > 90 || vm.siteData.defaultSFLightningParams.isDefaultChance > 90;
    for (item in vm.siteData.salesforceLocationParams) {
      if (vm.siteData.salesforceLocationParams[item]) {
        hasSFDomain = true;
      }
    }
    
    _isSalesforce = hasSFDomain && hasSFDomain;

    return _isSalesforce;
  };




  // Initialization
  vm.init();

}

homeCtrl.$inject = ['$scope', '$q', '$timeout'];

app.controller('homeCtrl', homeCtrl);