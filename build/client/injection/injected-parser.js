;(function() {



  // Check URL if it is a salesforce
  function checkSFLocation() {
    var _lightningForceRE   = /(http(s)?\:\/\/)(\w+\.)+(lightning\.force\.)(\w+)(.*)/;
    var _forceRE            = /(http(s)?\:\/\/)(\w+\.)+(force\.)(\w+)(.*)/;
    var _saleforceRE        = /(http(s)?\:\/\/)(\w+\.)+(salesforce\.)(\w+)(.*)/;
    var _location           = window.location.href;

    var _matching = {
      isLightningForce  : _lightningForceRE.test(_location),
      isForce           :          _forceRE.test(_location),
      isSalesforce      :      _saleforceRE.test(_location)
    };
    return _matching;
  }





  // Checking default Aura tags provided on page
  function checkAuraTags() {
    var auraTagsData = {
      auraAppcacheProgress  : !!document.getElementById('auraAppcacheProgress'),
      auraLoadingBox        : !!document.getElementById('auraLoadingBox'),
      auraError             : !!document.getElementById('auraError')
    };
    return auraTagsData;
  }





  // Checking on default elements of salesforce classic
  function checkDefaultSalesforceLightning() {
    var item;
    var _isDefaultChance = 0;
    var _defaultSFElements = {
      hasHeader: !!document.getElementById('oneHeader')
    };
    var _auraTags = checkAuraTags();
    for (item in _auraTags) {
      if (_auraTags[item]) {
        _isDefaultChance += 10;
      }
    }
    if (_defaultSFElements.hasHeader) {
      _isDefaultChance += 70;
    }
    return { isDefaultChance: _isDefaultChance, defaultSFElements: _defaultSFElements };
  }





  // Checking on default elements of salesforce lightning
  function checkDefaultSalesforceClassic() {
    var item;
    var _isDefaultChance = 0;
    var _defaultSFElements = {
      hasHeader   : !!document.getElementById('AppBodyHeader'),
      hasSidebar  : !!document.getElementById('sidebarCell'),
      hasBody     : !!document.getElementById('bodyCell')
    };

    for (item in _defaultSFElements) {
      if (_defaultSFElements[item]) {
        _isDefaultChance += 33;
      }
    }
    return { isDefaultChance: _isDefaultChance, defaultSFElements: _defaultSFElements };
  }





  // check default community url, should exist "/s/" after site origin
  function checkLocation() {
    var _location = window.location.href;
    var locationRE = /(http(s)?\:\/\/)(\w+\.)+(\w+)(\/s\/)(.*)/;
    return locationRE.test(_location);
  }







  // Script initialization
  function initScript() {
    
    var messageObject = {
      isCommunityLocation       : checkLocation(),
      salesforceLocationParams  : checkSFLocation(),
      defaultSFLightningParams  : checkDefaultSalesforceLightning(),
      defaultSFClassicParams    : checkDefaultSalesforceClassic(),
      hasAuraTags               : checkAuraTags(),

      action: 'getInformation'
    };

    chrome.runtime.sendMessage(messageObject);
  }





  // Call initialization
  initScript();

  
  // document.addEventListener('DOMContentLoaded', initScript);

}());