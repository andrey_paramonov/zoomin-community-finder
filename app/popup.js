// You need to explicitly add URL protocols to Angular's whitelist using a regular expression. 
// Only http, https, ftp and mailto are enabled by default. 
// Angular will prefix a non-whitelisted URL with unsafe: when using a protocol such as chrome-extension:.
var app = angular.module("myApp", ["ui.router"]).config(['$compileProvider', function( $compileProvider ) {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
    }
]);


app.config(function($stateProvider) {

    $stateProvider
    .state('home', {
      url: '/',
      views: {
        'main': {
          controller: 'homeCtrl',
          controllerAs: 'vm',
          templateUrl: 'client/home/home.html'
        }
      }
    });

});


app.controller('myCtrl', function($state) {
    $state.go('home');
});